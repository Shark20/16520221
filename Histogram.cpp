#include<iostream>
#include"opencv2/highgui/highgui.hpp"
#include "opencv2\opencv.hpp"
#include"opencv2/imgproc/imgproc.hpp"
#include<string>


using namespace std;
using namespace cv;
void Histogram(Mat &mau) {
	vector<Mat> bgr_planes;
	split(mau, bgr_planes);
	//thiet lap so luong thung
	int histSize = 256;
	//dat pham vi (cho B, G, R)
	float range[] = { 0, 256 };
	const float* histRange = { range };
	//dat tham so bieu do
	bool uniform = true, accumulate = false;
	//tinh toan bieu do
	Mat b_hist, g_hist, r_hist;
	calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);
	//ve bieu do B,G,R
	int hist_w = 512, hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);
	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));
	//binh thuong ket qua thanh(0, histImage.rows)
	normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	//ve moi kenh mau
	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))), Point(bin_w*(i), hist_h - cvRound(b_hist.at<float>(i))),Scalar(255, 0, 0), 2, 8, 0);
	    line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))), Point(bin_w*(i), hist_h - cvRound(g_hist.at<float>(i))),Scalar(0, 255, 0), 2, 8, 0);
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))), Point(bin_w*(i), hist_h - cvRound(r_hist.at<float>(i))),Scalar(0, 0, 255), 2, 8, 0);
	}
	imshow("Histogram ", histImage);
}
Mat Equalize(const Mat & inputImage) {
	if (inputImage.channels() >= 3)
	{
		vector<Mat> channels;
		split(inputImage, channels);
		Mat B, G, R;

		equalizeHist(channels[0], B);
		equalizeHist(channels[1], G);
		equalizeHist(channels[2], R);
		vector<Mat> combined;
		combined.push_back(B);
		combined.push_back(G);
		combined.push_back(R);
		Mat result;
		merge(combined, result);
		return result;
	}
	return Mat();
}
int main()
{
	string duongDan, ten_file;
	char* equalized_window = "Hinh anh duoc can bang";
	
	cout << "nhap duong dan: \n";
	cin >> duongDan;
	cin.ignore();
	cout << "ten file(co duoi .jpg) : ";//ten file phai co ".jpg"
	cin >> ten_file;
	duongDan += "/" + ten_file;
	//
	Mat mau = imread(duongDan);
	
	
	/*====== can bang theo Y, Cr, Cb ======= 
	Mat ketqua;
	cvtColor(lena, ketqua, COLOR_BGR2YCrCb);
	vector<Mat> vec_channels;
	split(ketqua, vec_channels);

	equalizeHist(vec_channels[0], vec_channels[0]);
	merge(vec_channels, ketqua);

	//Convert the histogram equalized image from YCrCb to BGR color space again
	cvtColor(ketqua, ketqua, COLOR_YCrCb2BGR);
	*/
	namedWindow(equalized_window, CV_WINDOW_AUTOSIZE);

    //xuat
	imshow("Mau", mau);
	Mat a = Equalize(mau);
	imshow(equalized_window,a );
	Histogram(a);
	waitKey(0);

	return 0;

}