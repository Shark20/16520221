#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	float P;
	int x, y;
	x = 0;
	y = b;
	P = 2 * (b*b) / (a*a) - 2 * b + 1;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a+b*b)<=a*a*a*a)
	{
		
		if (P < 0)
		{
			P = P + 2 * ((float)(b*b) / (a*a)*(2 * x + 3));
		}
		else {
			P = P - 4 * y + 2 * ((float)((b*b) / (a*a))*(2 * x + 3));
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = a;
	y = 0;
	P= 2 * (a*a) / (b*b) - 2 * a + 1;
	Draw4Points(xc, xc, x, y, ren);
	while (x*x*(a*a + b*b)>=a*a*a*a)
	{
		
		if (P < 0)
		{
			P += 4 * a*a*y + 6 * a*a;
		}
		else {
			P += 4 * a*a*y - 4 * b*b*x + 4 * b*b + 6 * a*a;
			x--;
		}
		y++;
		Draw4Points(xc, xc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	float P;
	double z1, z2, z3, z4;
	int x, y;
	x = 0;
	y = b;
	z1 = 2 * ((float)(b*b) / (a*a)*(2 * x + 3));
	z2= - 4 * y + 2 * ((float)((b*b) / (a*a))*(2 * x + 3));
	P = 2 * (b*b) / (a*a) - 2 * b + 1;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{

		if (P < 0)
		{
			P = P + z1;
		}
		else {
			P = P + z2;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
    x = a;
	y = 0;
	z3 = 4 * a*a*y + 6 * a*a;
	z4 = 4 * a*a*y - 4 * b*b*x + 4 * b*b + 6 * a*a;
	P= 2 * (a*a) / (b*b) - 2 * a + 1;
	Draw4Points(xc, xc, x, y, ren);
	while (x*x*(a*a + b*b)>=a*a*a*a)
	{
		
		if (P < 0)
		{
			P += z3;
		}
		else {
			P += z4;
			x--;
		}
		y++;
		Draw4Points(xc, xc, x, y, ren);
	}
}