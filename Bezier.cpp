#include "Bezier.h"
#include <iostream>
using namespace std;
int tinhGiaiThua(int a) {
	int kq=1;
	for (int i = 1; i <= a; i++)
		kq *= i;
	return kq;
}
int tinhNhiThuc(int n, int k) {
	float t;
	float B;
	B = (tinhGiaiThua(n) / (tinhGiaiThua(k)*(tinhGiaiThua(n - k))))* pow((1-t), (n - k))*t;
	return B;
}

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	for (float t = 0.0; t < 1; t += 0.005) {
		double xt = tinhNhiThuc(2, 0)*p1.x + tinhNhiThuc(2, 1)*p2.x + tinhNhiThuc(2, 2)*p3.x;
		double yt = tinhNhiThuc(2, 0)*p1.y + tinhNhiThuc(2, 1)*p2.y + tinhNhiThuc(2, 2)*p3.y;
		SDL_RenderDrawPoint(ren, xt, yt);
	}

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	for (float t = 0.0; t < 1; t += 0.005) {
		double xt = tinhNhiThuc(3, 0)*p1.x + tinhNhiThuc(3, 1)*p2.x + tinhNhiThuc(3, 2)*p3.x + tinhNhiThuc(3, 3)*p4.x;
		double yt = tinhNhiThuc(3, 0)*p1.y + tinhNhiThuc(3, 1)*p2.y + tinhNhiThuc(3, 2)*p3.y + tinhNhiThuc(3, 3)*p4.x;
		SDL_RenderDrawPoint(ren, xt, yt);
	}

}
